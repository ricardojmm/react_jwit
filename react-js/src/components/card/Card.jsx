import styled from 'styled-components';

const Container = styled.div`
    height: 150px;
    width: 350px;
    border-radius: 5px;
    background-color: #fff0ea;
    
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

const Title = styled.p`
    color: #f06f37;
    text-transform: uppercase;
    font-size: 15px;
    font-width: bold;
    font-family: -apple-system, BlinkMaskSystemFont, 'Segoe UI', Roboto;
    width: 300px;
    text-align: center;
`;

const Button = styled.button`
    background-color: #f06f37;
    text-transform: uppercase;
    border: 1px solid #f06f37;
    padding: 7px 25px;
    color: white;
    font-weight: bold;
    border-radius: 15px;
    
    opacity: .9;
    cursor: pointer;
`;

const Card = () => {
    return (
        <Container>
            <Title>
                use code: pigi100 for rs.100 off on your first order!
            </Title>
            <Button>
                grab now
            </Button>
        </Container>
    );
}

export default Card;