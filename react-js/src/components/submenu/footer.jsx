import styled from 'styled-components';

import { Circulo, Icon } from './Option';

import cancel from '../../images/cancel.svg';
import check from '../../images/check.svg';
import refresh from '../../images/refresh.svg';

const Container = styled.div`
    display: flex;
    justify-content: center;
    aling-items: center;
    flex-direction: row;
`;

const Containerv2 = styled.div`
    display: flex;
    justify-content: center;
    aling-items: center;
    flex-direction: row;
    margin-top: 15px;
    padding: 25px 0;
    border-top: 1px solid #ffffff80;
`;

const Item = ({ v, onClick }) => {
    return (
        <Circulo visible={true} style={{ height: 60, width: 60, marginLeft: 15 }} onClick={onClick}>
            <Icon src={v} visible={true} style={{ height: 60, width: 60 }} />
        </Circulo>
    );
}

const Footer = ({ version, onReset, onCancel, onAcept, id }) => {

    const handleReset = () => {
        if (typeof (onReset) === 'function') {
            onReset(id);
        }
    }

    const handleOk = () => {
        if (typeof (onAcept) === 'function') {
            onAcept();
        }
    }

    const handleCancel = () => {
        if (typeof (onCancel) === 'function') {
            onCancel();
        }
    }


    if (version === 1) {
        const data = [
            { icon: cancel, onClick: handleCancel },
            { icon: check, onClick: handleOk }
        ];

        return <Container>
            {
                data.map((v, i) => (
                    <Item v={v.icon} onClick={v.onClick} />
                ))
            }
        </Container>;
    }

    if (version === 2) {
        const data = [
            { icon: cancel, onClick: handleCancel },
            { icon: refresh, onClick: handleReset },
            { icon: check, onClick: handleOk }
        ];

        return <Containerv2>
            {
                data.map((v, i) => (
                    <Item key={i} v={v.icon} onClick={v.onClick} />
                ))
            }
        </Containerv2>;
    }
    return <></>;

}

export default Footer;