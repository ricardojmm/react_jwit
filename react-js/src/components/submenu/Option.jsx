import styled from 'styled-components';

import check from '../../images/check.svg';

const Container = styled.div`
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`;

const Title = styled.p`
    color: white;
    font-weight: 500;
    font-family: -apple-system, BlinkMaskSystemFont, 'Segoe UI', Roboto;
    margin: 0;
    text-transform: uppercase;
`;

export const Circulo = styled.div`
    border-radius: 50%;
    border: 1.5px solid white;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: ${props => props.visible ? 'white' : 'transparent'};
`;

export const Icon = styled.img`
    height: 35px;
    width: 35px;
    color: green;
    visibility: ${props => props.visible ? 'visible' : 'hidden'};
`;

const Option = ({ title, state, onChange }) => {

    const handleChange = () => {
        if (typeof (onChange) === 'function') onChange(title);
    }

    return (
        <Container>
            <Title>
                {title}
            </Title>
            <Circulo visible={state} onClick={handleChange}>
                <Icon src={check} visible={state} />
            </Circulo>
        </Container>
    );
}

export default Option;