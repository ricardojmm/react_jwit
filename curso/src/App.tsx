import styled from "styled-components";
// import Menu from "react-menu";
import Menu from "react-menu";

import check from './images/check.svg';
import reset from './images/refresh.svg';
import cancel from './images/cancel.svg';

const Container = styled.div`
`;

const resources = {
  check: check,
  refresh: reset,
  cancel: cancel
};

function App(): JSX.Element {

  return (
    <Container>
      <Menu
        onAcept={e => console.log(e)}
        onCancel={() => console.log('SELECCION CANCELADA')}
        resources={resources}
      />

      <Menu
        onAcept={e => console.log(e)}
        backgroundColor="#21d0d0" options={[
          'price low to high',
          'price high to low',
          'popularity'
        ]
        }
        onCancel={() => console.log('SELECCION CANCELADA')}
        resources={resources}
      />
    </Container>
  );
}

export default App;
