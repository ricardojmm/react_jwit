"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
var checkBox_1 = __importDefault(require("./checkBox"));
var Container = styled_components_1.default.div(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    margin-left: 25px;\n    margin-top: 15px;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    margin-right: 25px;\n    align-items: center;\n"], ["\n    margin-left: 25px;\n    margin-top: 15px;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    margin-right: 25px;\n    align-items: center;\n"])));
var Title = styled_components_1.default.p(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n    color: white;\n    font-weight: 500;\n    font-family: -apple-system, BlinkMaskSystemFont, 'Segoe UI', Roboto;\n    margin: 0;\n    text-transform: uppercase;\n"], ["\n    color: white;\n    font-weight: 500;\n    font-family: -apple-system, BlinkMaskSystemFont, 'Segoe UI', Roboto;\n    margin: 0;\n    text-transform: uppercase;\n"])));
var Option = function (params) {
    var handleClick = function (state) {
        if (typeof (params.onClick) === 'function')
            params.onClick({
                value: params.value,
                state: state
            });
    };
    return (react_1.default.createElement(Container, null,
        react_1.default.createElement(Title, null, params.value),
        react_1.default.createElement(checkBox_1.default, { onClick: handleClick, state: params.defaultState, image: params.check })));
};
exports.default = Option;
var templateObject_1, templateObject_2;
