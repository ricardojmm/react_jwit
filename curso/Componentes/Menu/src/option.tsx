import React from 'react';
import styled from 'styled-components';
import CheckBox from './checkBox';
import { resources } from './types';

// import check from '../images/check.svg';

const Container = styled.div`
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`;

const Title = styled.p`
    color: white;
    font-weight: 500;
    font-family: -apple-system, BlinkMaskSystemFont, 'Segoe UI', Roboto;
    margin: 0;
    text-transform: uppercase;
`;

export interface Params {
    value: string,
    defaultState: boolean,
    onClick?: (params: { value: string, state: boolean }) => void;
    check: string;
}

const Option = (params: Params): JSX.Element => {

    const handleClick = (state: boolean) => {
        if (typeof (params.onClick) === 'function')
            params.onClick({
                value: params.value,
                state
            });
    }

    return (
        <Container>
            <Title>
                {params.value}
            </Title>
            <CheckBox onClick={handleClick} state={params.defaultState} image={params.check} />
        </Container>
    );
}

export default Option;