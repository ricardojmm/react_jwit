"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Icon = exports.Container = void 0;
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Container = styled_components_1.default.div(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    border-radius: 50%;\n    border: 1.5px solid white;\n    height: 35px;\n    width: 35px;\n    cursor: pointer;\n    background-color: ", ";\n"], ["\n    border-radius: 50%;\n    border: 1.5px solid white;\n    height: 35px;\n    width: 35px;\n    cursor: pointer;\n    background-color: ", ";\n"])), function (props) { return props.visible ? 'white' : 'transparent'; });
exports.Icon = styled_components_1.default.img(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n    height: 35px;\n    width: 35px;\n    color: green;\n    visibility: ", ";\n"], ["\n    height: 35px;\n    width: 35px;\n    color: green;\n    visibility: ", ";\n"])), function (props) { return props.visible ? 'visible' : 'hidden'; });
var CheckBox = function (params) {
    var handleClick = function () {
        if (typeof (params.onClick) === 'function')
            params.onClick(!params.state);
    };
    return react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(exports.Container, { visible: params.state, onClick: handleClick },
            react_1.default.createElement(exports.Icon, { src: params.image, visible: params.state })));
};
CheckBox.defaultProps = {
    state: false
};
exports.default = CheckBox;
var templateObject_1, templateObject_2;
