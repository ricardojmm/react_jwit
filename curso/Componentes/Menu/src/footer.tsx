import React from "react";
import styled from "styled-components";
import Btn from "./btn";
import { resources } from "./types";

const cancel = '/public/images/cancel.svg';
const check = '/public/images/check.svg';
const refresh = '/public/images/refresh.svg';

const Container = styled.div`
    display: flex;
    justify-content: space-evenly;
    aling-items: center;
    flex-direction: row;
    margin-top: 15px;
    padding 15px 55px 15px 55px;
    border-top: 1px solid #ffffff80;
`;

interface Option {
    icon: string;
    onClick: () => void
}

export interface Params {
    enableReset?: boolean;
    enableCancel?: boolean;
    onReset?: (value: number) => void;
    onAcept?: () => void;
    onCancel?: () => void;
    resources: resources;
}

const Footer = (params: Params): JSX.Element => {

    const handleReset = () => {
        if (typeof (params.onReset) === 'function') {
            params.onReset(1);
        }
    }

    const handleAcept = () => {
        if (typeof (params.onAcept) === 'function') {
            params.onAcept();
        }
    }

    const handleCancel = () => {
        if (typeof (params.onCancel) === 'function') {
            params.onCancel();
        }
    }

    const data: Option[] = [
        { icon: params.resources.check, onClick: handleAcept }

    ];

    if (params.enableReset)
        data.push({ icon: params.resources.refresh, onClick: handleReset });

    if (params.enableCancel)
        data.push({ icon: params.resources.cancel, onClick: handleCancel });

    return (
        <Container>
            {
                data.map((v, i) => (
                    <Btn key={i} image={v.icon} onClick={v.onClick} />
                ))
            }
        </Container>
    );
}

Footer.defaultProps = {
    enableReset: true,
    enableCancel: true
}

export default Footer;