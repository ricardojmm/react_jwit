import React from "react";
import styled from "styled-components";

// import check from '../images/check.svg';

export const Container = styled.div`
    border-radius: 50%;
    border: 1.5px solid white;
    height: 60px;
    width: 60px;
    cursor: pointer;
    background-color:white;
    margin-left: 15px;
`;

export const Icon = styled.img`
    height: 100%;
    width: 100%;
    object-fit: contain;
    visibility: visible;
`;

export interface Params {
    image: string;
    onClick?: () => void
}

const Btn = (params: Params): JSX.Element => {
    const handleClick = () => {
        if (typeof (params.onClick) === 'function') params.onClick();
    }

    return <>
        <Container onClick={handleClick}>
            <Icon src={params.image} />
        </Container>
    </>;
}

export default Btn;