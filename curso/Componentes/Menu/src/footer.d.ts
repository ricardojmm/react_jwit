/// <reference types="react" />
import { resources } from "./types";
export interface Params {
    enableReset?: boolean;
    enableCancel?: boolean;
    onReset?: (value: number) => void;
    onAcept?: () => void;
    onCancel?: () => void;
    resources: resources;
}
declare const Footer: {
    (params: Params): JSX.Element;
    defaultProps: {
        enableReset: boolean;
        enableCancel: boolean;
    };
};
export default Footer;
