import React from "react";
import styled from "styled-components";

// import check from '../images/check.svg';

export const Container = styled.div<{ visible: boolean }>`
    border-radius: 50%;
    border: 1.5px solid white;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: ${props => props.visible ? 'white' : 'transparent'};
`;

export const Icon = styled.img<{ visible: boolean }>`
    height: 35px;
    width: 35px;
    color: green;
    visibility: ${props => props.visible ? 'visible' : 'hidden'};
`;

export interface Params {
    image: string;
    onClick?: (value: boolean) => void;
    state: boolean;
}

const CheckBox = (params: Params): JSX.Element => {
    const handleClick = () => {
        if (typeof (params.onClick) === 'function') params.onClick(!params.state);
    }

    return <>
        <Container visible={params.state} onClick={handleClick}>
            <Icon src={params.image} visible={params.state} />
        </Container>
    </>;
}

CheckBox.defaultProps = {
    state: false
}

export default CheckBox;