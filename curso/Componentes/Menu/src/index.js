"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
var footer_1 = __importDefault(require("./footer"));
var option_1 = __importDefault(require("./option"));
var Container = styled_components_1.default.div(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    margin-top: 15px;\n    background-color: ", ";\n    border-radius: 25px;\n\n    //height: 200px;\n    width: 400px;\n    padding-top: 15px;\n"], ["\n    margin-top: 15px;\n    background-color: ", ";\n    border-radius: 25px;\n\n    //height: 200px;\n    width: 400px;\n    padding-top: 15px;\n"])), function (props) { return props.color; });
var Menu = function (params) {
    var _a = (0, react_1.useState)([]), options = _a[0], setOptions = _a[1];
    (0, react_1.useEffect)(function () {
        var raw = [];
        params.options.forEach(function (el) { return raw.push({
            value: el,
            state: false
        }); });
        setOptions(raw);
    }, [params.options]);
    var handleClick = function (e) {
        var copy = __spreadArray([], options, true);
        var index = copy.findIndex(function (f) { return f.value === e.value; });
        if (index > -1)
            copy[index] = e;
        setOptions(copy);
    };
    var handleReset = function () {
        var raw = [];
        params.options.forEach(function (el) { return raw.push({
            value: el,
            state: false
        }); });
        setOptions(raw);
    };
    var handleAcept = function () {
        if (typeof (params.onAcept) === 'function')
            params.onAcept(options);
    };
    var handleCancel = function () {
        if (typeof (params.onCancel) === 'function')
            params.onCancel();
    };
    return (react_1.default.createElement(Container, { color: params.backgroundColor },
        options.map(function (v, i) { return react_1.default.createElement(option_1.default, { key: i, value: v.value, defaultState: v.state, onClick: handleClick }); }),
        react_1.default.createElement(footer_1.default, { resources: params.resources, onAcept: handleAcept, onReset: handleReset, onCancel: handleCancel })));
};
Menu.defaultProps = {
    options: [
        '1up nutrition',
        'asitis',
        'avatar',
        'big muscles',
        'bpi sports',
        'bsn',
        'cellucor',
        'dymatize',
    ],
    backgroundColor: '#ff7745'
};
exports.default = Menu;
var templateObject_1;
