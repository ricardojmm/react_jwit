"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Icon = exports.Container = void 0;
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Container = styled_components_1.default.div(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    border-radius: 50%;\n    border: 1.5px solid white;\n    height: 60px;\n    width: 60px;\n    cursor: pointer;\n    background-color:white;\n    margin-left: 15px;\n"], ["\n    border-radius: 50%;\n    border: 1.5px solid white;\n    height: 60px;\n    width: 60px;\n    cursor: pointer;\n    background-color:white;\n    margin-left: 15px;\n"])));
exports.Icon = styled_components_1.default.img(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n    height: 100%;\n    width: 100%;\n    object-fit: contain;\n    visibility: visible;\n"], ["\n    height: 100%;\n    width: 100%;\n    object-fit: contain;\n    visibility: visible;\n"])));
var Btn = function (params) {
    var handleClick = function () {
        if (typeof (params.onClick) === 'function')
            params.onClick();
    };
    return react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(exports.Container, { onClick: handleClick },
            react_1.default.createElement(exports.Icon, { src: params.image })));
};
exports.default = Btn;
var templateObject_1, templateObject_2;
