/// <reference types="react" />
import { resources } from "./src/types";
interface Params {
    options: string[];
    backgroundColor?: string;
    onAcept?: (options: option[]) => void;
    onCancel?: () => void;
    resources: resources;
}
export interface option {
    value: string;
    state: boolean;
}
declare const Menu: {
    (params: Params): JSX.Element;
    defaultProps: {
        options: string[];
        backgroundColor: string;
    };
};
export default Menu;
