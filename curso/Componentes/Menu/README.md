## Installation & Usage

```sh
npm install @react/menu --save
```

### Include the component
```js
import App from '@react/menu';

const App = () => {
    return <App />
}
```