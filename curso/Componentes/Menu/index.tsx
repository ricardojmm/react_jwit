import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Footer from "./src/footer";
import Option, { Params as paramsComponentOption } from "./src/option";
import { resources } from "./src/types";

const Container = styled.div`
    margin-top: 15px;
    background-color: ${props => props.color};
    border-radius: 25px;

    //height: 200px;
    width: 400px;
    padding-top: 15px;
`;

interface Params {
    options: string[];
    backgroundColor?: string;
    onAcept?: (options: option[]) => void;
    onCancel?: () => void;
    resources: resources;
}

export interface option {
    value: string;
    state: boolean;
}

const Menu = (params: Params): JSX.Element => {
    const [options, setOptions] = useState<{
        value: string;
        state: boolean;
    }[]>([]);

    useEffect(() => {
        var raw: option[] = [];
        params.options.forEach(el => raw.push({
            value: el,
            state: false
        }));
        setOptions(raw);
    }, [params.options]);

    const handleClick: paramsComponentOption['onClick'] = (e) => {
        const copy: option[] = [...options];

        const index: number = copy.findIndex(f => f.value === e.value);

        if (index > -1)
            copy[index] = e;

        setOptions(copy);
    }

    const handleReset = () => {
        var raw: option[] = [];
        params.options.forEach(el => raw.push({
            value: el,
            state: false
        }));
        setOptions(raw);
    }

    const handleAcept = () => {
        if (typeof (params.onAcept) === 'function') params.onAcept(options);
    };

    const handleCancel = () => {
        if (typeof (params.onCancel) === 'function') params.onCancel();
    }

    return (
        <Container color={params.backgroundColor}>
            {options.map((v, i) => <Option check={params.resources.check} key={i} value={v.value} defaultState={v.state} onClick={handleClick} />)}

            <Footer resources={params.resources} onAcept={handleAcept} onReset={handleReset} onCancel={handleCancel} />
        </Container>
    );
}

Menu.defaultProps = {
    options: [
        '1up nutrition',
        'asitis',
        'avatar',
        'big muscles',
        'bpi sports',
        'bsn',
        'cellucor',
        'dymatize',
    ],
    backgroundColor: '#ff7745'
}

export default Menu