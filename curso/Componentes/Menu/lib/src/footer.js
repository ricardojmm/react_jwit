"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
var btn_1 = __importDefault(require("./btn"));
var cancel = '/public/images/cancel.svg';
var check = '/public/images/check.svg';
var refresh = '/public/images/refresh.svg';
var Container = styled_components_1.default.div(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    display: flex;\n    justify-content: space-evenly;\n    aling-items: center;\n    flex-direction: row;\n    margin-top: 15px;\n    padding 15px 55px 15px 55px;\n    border-top: 1px solid #ffffff80;\n"], ["\n    display: flex;\n    justify-content: space-evenly;\n    aling-items: center;\n    flex-direction: row;\n    margin-top: 15px;\n    padding 15px 55px 15px 55px;\n    border-top: 1px solid #ffffff80;\n"])));
var Footer = function (params) {
    var handleReset = function () {
        if (typeof (params.onReset) === 'function') {
            params.onReset(1);
        }
    };
    var handleAcept = function () {
        if (typeof (params.onAcept) === 'function') {
            params.onAcept();
        }
    };
    var handleCancel = function () {
        if (typeof (params.onCancel) === 'function') {
            params.onCancel();
        }
    };
    var data = [
        { icon: params.resources.check, onClick: handleAcept }
    ];
    if (params.enableReset)
        data.push({ icon: params.resources.refresh, onClick: handleReset });
    if (params.enableCancel)
        data.push({ icon: params.resources.cancel, onClick: handleCancel });
    return (react_1.default.createElement(Container, null, data.map(function (v, i) { return (react_1.default.createElement(btn_1.default, { key: i, image: v.icon, onClick: v.onClick })); })));
};
Footer.defaultProps = {
    enableReset: true,
    enableCancel: true
};
exports.default = Footer;
var templateObject_1;
