/// <reference types="react" />
export declare const Container: import("styled-components").StyledComponent<"div", any, {
    visible: boolean;
}, never>;
export declare const Icon: import("styled-components").StyledComponent<"img", any, {
    visible: boolean;
}, never>;
export interface Params {
    image: string;
    onClick?: (value: boolean) => void;
    state: boolean;
}
declare const CheckBox: {
    (params: Params): JSX.Element;
    defaultProps: {
        state: boolean;
    };
};
export default CheckBox;
