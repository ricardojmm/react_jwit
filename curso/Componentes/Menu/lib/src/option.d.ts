/// <reference types="react" />
export interface Params {
    value: string;
    defaultState: boolean;
    onClick?: (params: {
        value: string;
        state: boolean;
    }) => void;
    check: string;
}
declare const Option: (params: Params) => JSX.Element;
export default Option;
