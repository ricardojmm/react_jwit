/// <reference types="react" />
export declare const Container: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const Icon: import("styled-components").StyledComponent<"img", any, {}, never>;
export interface Params {
    image: string;
    onClick?: () => void;
}
declare const Btn: (params: Params) => JSX.Element;
export default Btn;
