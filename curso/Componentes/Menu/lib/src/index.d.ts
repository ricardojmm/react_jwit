/// <reference types="react" />
interface Params {
    options: string[];
    backgroundColor?: string;
    onAcept?: (options: option[]) => void;
    onCancel?: () => void;
    resources: {
        cancel: string;
        check: string;
        refresh: string;
    };
}
export interface option {
    value: string;
    state: boolean;
}
declare const Menu: {
    (params: Params): JSX.Element;
    defaultProps: {
        options: string[];
        backgroundColor: string;
    };
};
export default Menu;
